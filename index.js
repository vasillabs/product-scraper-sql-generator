const fs = require('fs')
const http = require('http')
const request = require('request')
const products = require('./products.json')

let imageArray = []

const generateSQLqueries = function () {
    products.forEach(product => {
        const productData = {
            title : product.product_title,
            description : product.product_description.replace(/'/g, "''"),
            ingredients : product.product_ingredients,
            wieght : product.product_weight,
            price : product.product_price_USD
        }

        const imageURL = product.product_img_url
        const lastSlashIndex = imageURL.lastIndexOf('/')
        const imageName = imageURL.substring(lastSlashIndex + 1)
        const pathToImageFolder = `./images/${imageName}`
        const imageData = {
            name: pathToImageFolder,
            url: product.imageURL
        }
        
        productData.image = pathToImageFolder
        imageArray.push(imageData)
        buildSQLstring(productData)
    })
}

const buildSQLstring = function(product) {
    const SQLValues = {
        db: 'stemacell',
        table: 'products',
        product: {
            vendorid: '1',
            productCode: null,
            priceCAD: null,
            priceEUR: null,
            priceGBP: null,
            thumbnail: null
        },
        setNull: null
    }

    const weightString = `${product.weight == '' ? `${setNull}` : `${product.weight}`}`;
    const SQLstring = `INSERT INTO \`${SQLValues.db}\`.\`${SQLValues.table}\` (\`productname\`, \`productcode\`, \`priceUSD\`, \`priceCAD\`, \`priceEUR\`, \`priceGBP\`, \`description\`, \`weight\`, \`image\`, \`thumbnail\`, \`vendorid\`) 
        VALUES ('${product.title}', ${SQLValues.product.productCode},  ${product.price}, ${SQLValues.product.priceCAD}, ${SQLValues.product.priceEUR}, ${SQLValues.product.priceGBP}, '${product.description}',  '${weightString}', '${product.image}', ${SQLValues.product.thumbnail}, ${SQLValues.vendorid}); \n`
    
    fs.appendFile('SQLqueries.sql', SQLstring, function (err) {
        if (err) throw err;
    })
}

generateSQLqueries()

/* -- UNCOMMENT LATER --- */
const download = function (uri, filename, callback) {
    request.head(uri, function (err, res, body) {
          console.log('content-type:', res.headers['content-type']);
          console.log('content-length:', res.headers['content-length']);
        request(uri).pipe(fs.createWriteStream('./images/' + filename)).on('close', callback);
    });
};

imageArray.forEach(img => {
    download(img.url, img.name, function () {
        console.log('done');
    });
});